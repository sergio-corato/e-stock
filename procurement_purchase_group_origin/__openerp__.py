# -*- coding: utf-8 -*-
# Copyright 2020 Sergio Corato <https://github.com/sergiocorato>
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "Procurement Purchase Group by Origin",
    "version": "8.0.1.0.0",
    "author": "Sergio Corato",
    "website": "https://efatto.it",
    "category": "Procurements",
    "depends": ['purchase',
                'procurement',
                ],
    "installable": True
}
