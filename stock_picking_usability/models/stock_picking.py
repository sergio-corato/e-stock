# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields, exceptions


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    sale_partner_invoice_id = fields.Many2one(
        related='sale_id.partner_invoice_id')
    sale_partner_id = fields.Many2one(
        related='sale_id.partner_id')


class StockPickingPackagePreparation(models.Model):
    _inherit = 'stock.picking.package.preparation'

    @api.onchange('picking_ids')
    def update_partner_from_picking(self):
        for sppp in self:
            if sppp.picking_ids:
                if sppp.picking_ids[0].sale_partner_invoice_id:
                    sppp.partner_invoice_id = sppp.picking_ids[0].\
                        sale_partner_invoice_id
                if sppp.picking_ids[0].sale_partner_id:
                    sppp.partner_id = sppp.picking_ids[0].sale_partner_id
                sppp.partner_shipping_id = sppp.picking_ids[0].partner_id

                if sppp.picking_ids.filtered(
                    lambda x: x.partner_id != sppp.partner_shipping_id
                ):
                    raise exceptions.ValidationError(
                        'More than one shipping partner found in transfers')

                if any(sppp.picking_ids.mapped('sale_partner_invoice_id')) \
                    and sppp.picking_ids.filtered(
                        lambda x: x.sale_partner_invoice_id !=
                        sppp.partner_invoice_id
                ):
                    raise exceptions.ValidationError(
                        'More than one invoice partner found in transfers')

    @api.multi
    def write(self, vals):
        # add a warning if shipping address differ between sppp and pickings
        if 'picking_ids' in vals and vals['picking_ids'][0][2]:
            shipping_address_picking_id = self.env['stock.picking'].browse(
                vals['picking_ids'][0][2]).mapped('partner_id')
            for sppp in self:
                if 'partner_shipping_id' in vals:
                    if vals['partner_shipping_id'] != \
                            shipping_address_picking_id.id:
                        raise exceptions.ValidationError(
                            'Shipping partner in sppp differ from pickings!')
                # elif sppp.partner_shipping_id != shipping_address_picking_id:
                #     raise exceptions.ValidationError(
                #         'Shipping partner in sppp differ from pickings!')
        return super(StockPickingPackagePreparation, self).write(vals)
