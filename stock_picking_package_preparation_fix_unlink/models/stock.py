# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, exceptions, _


class StockPickingPackagePreparation(models.Model):
    _inherit = "stock.picking.package.preparation"

    @api.multi
    def unlink(self):
        if any(prep.state == 'done' for prep in self):
            raise exceptions.Warning(
                _('Cannot cancel a done package preparation.')
            )
        package_ids = self.mapped('package_id')
        if package_ids:
            package_ids.unlink()
        self.write({'state': 'cancel'})
