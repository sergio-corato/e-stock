# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, exceptions, _


class StockPickingPackagePreparation(models.Model):
    _inherit = "stock.picking.package.preparation"

    @api.multi
    def action_put_in_pack(self):
        for package in self:
            # ----- Check if package has details
            if not package.line_ids:
                raise exceptions.Warning(
                    _("Impossible to put in pack a package without details"))
            # ----- Assign ddt number if ddt type is set
            date_done = package.date
            if package.ddt_date_start:
                date_done = package.ddt_date_start
            for pick in package.picking_ids:
                for move in pick.move_lines:
                    move.date = date_done
                pick.date = date_done
        return super(StockPickingPackagePreparation, self).action_put_in_pack()
