# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields, exceptions, _


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def unlink(self):
        invoices = self.filtered(lambda x: x.state == 'draft')
        sppp_ids = invoices.mapped('stock_picking_package_preparation_ids').\
            filtered(lambda x: x.state != 'cancel')
        for ddt in sppp_ids:
            ddt.picking_ids.write({'invoice_state': '2binvoiced'})
        return super(AccountInvoice, self).unlink()
