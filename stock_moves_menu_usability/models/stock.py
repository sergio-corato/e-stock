# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields, exceptions


class StockMove(models.Model):
    _inherit = 'stock.move'
    _order = 'date desc'

    product_categ_id = fields.Many2one(related='product_id.categ_id')
