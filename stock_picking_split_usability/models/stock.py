# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields


class StockTransferDetails(models.TransientModel):
    _inherit = 'stock.transfer_details'

    @api.one
    def do_detailed_transfer(self):
        res = super(StockTransferDetails, self).do_detailed_transfer()
        self.picking_id.force_assign()
        return res


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def _get_splitted_move(self):
        splitted_move_ids = self.mapped('split_from')
        for move in self:
            move.is_splitted_move = False
            if move in splitted_move_ids:
                move.is_splitted_move = True

    qty_to_split = fields.Float()
    is_splitted_move = fields.Boolean(
        compute=_get_splitted_move,
    )

    @api.multi
    def split_qty_and_force_assign(self):
        for move in self:
            if move.qty_to_split <= 0.0:
                return
            qty_to_split = move.qty_to_split
            move.qty_to_split = 0
            new_move_id = move.split(move, qty_to_split)
            new_move = self.env['stock.move'].browse(new_move_id)
            new_move.force_assign()
