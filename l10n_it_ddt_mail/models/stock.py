# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, exceptions


class StockPickingPackagePreparation(models.Model):
    _inherit = "stock.picking.package.preparation"

    @api.multi
    def action_ddt_send(self):
        try:
            template_id = self.env['ir.model.data'].get_object_reference(
                'l10n_it_ddt_mail',
                'email_stock_ddt')[1]
        except exceptions:
            template_id = False
        try:
            compose_form_id = self.env['ir.model.data'].get_object_reference(
                'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        context = self.env.context.copy()
        context['default_model'] = 'stock.picking.package.preparation'
        context['default_res_id'] = self.id
        context['default_use_template'] = bool(template_id)
        context['default_template_id'] = template_id
        context['default_composition_mode'] = 'comment'
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': context,
        }
