# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.model
    def _get_invoice_line_vals(self, move, partner, inv_type):
        res = super(StockMove, self)._get_invoice_line_vals(
            move, partner, inv_type)
        if inv_type in ('out_invoice', 'out_refund') and not \
                move.procurement_id:
            # todo search if op of move is linked to other move with
            # procurement and get sale line id from them to copy discount and
            # price
            if len(move.linked_move_operation_ids) == 1:
                op = move.linked_move_operation_ids.operation_id
                linked_move_sale_ids = op.linked_move_operation_ids.filtered(
                    lambda x: [x for x in op.linked_move_operation_ids
                               if x.move_id.procurement_id and
                               x.move_id.procurement_id.sale_line_id and
                               x.move_id.product_id == move.product_id])
                if linked_move_sale_ids.filtered(
                    lambda x: x.move_id.procurement_id and
                        x.move_id.procurement_id.sale_line_id and
                        x.move_id != move and
                        x.move_id.product_id == move.product_id
                ):
                    sale_line = linked_move_sale_ids[0].move_id.\
                        procurement_id.sale_line_id
                    res['discount'] = sale_line.discount
                    # reuse code original function only after product !=
                    res['price_unit'] = sale_line.price_unit
                    uos_coeff = move.product_uom_qty and move.product_uos_qty\
                        / move.product_uom_qty or 1.0
                    res['price_unit'] = res['price_unit'] / uos_coeff
        return res
