# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api


class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.model
    def _prepare_values_extra_move(self, op, product, remaining_qty):
        res = super(StockMove, self)._prepare_values_extra_move(
            op, product, remaining_qty)
        picking = op.picking_id
        if picking.sale_id and 'partner_id' not in res:
            res['partner_id'] = picking.sale_id.partner_id.id
        return res
