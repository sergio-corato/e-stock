# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, api, fields, exceptions


class StockMove(models.Model):
    _inherit = 'stock.move'

    sale_line_id = fields.Many2one(
        related='procurement_id.sale_line_id')
    sale_price_unit = fields.Float(
        related='sale_line_id.price_unit')


class StockPickingPackagePreparation(models.Model):
    _inherit = 'stock.picking.package.preparation'

    print_prices = fields.Boolean()
