# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields


class DdTCreateInvoice(models.TransientModel):
    _inherit = "ddt.create.invoice"

    def _get_sale_journal_by_sequence(self):
        return self.env['account.journal'].search([
            ('type', '=', 'sale')
        ], order='sequence ASC', limit=1)

    journal_id = fields.Many2one(
        default=_get_sale_journal_by_sequence)
