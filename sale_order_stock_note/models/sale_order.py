# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _, exceptions


class SaleOrder(models.Model):
    _inherit = "sale.order"

    stock_note = fields.Text('Note for stock picking')

    @api.multi
    def action_ship_create(self):
        res = super(SaleOrder, self).action_ship_create()
        for order in self:
            if order.stock_note and order.picking_ids:
                order.picking_ids.write({'note': order.stock_note})
        return res
