# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import fields, models, exceptions, api, _, sql_db
import base64
import csv
import cStringIO
import logging
from datetime import datetime


class ProductImport(models.Model):
    _name = 'product.import'

    date = fields.Datetime(default=fields.Datetime.now())
    data = fields.Binary('File', required=True)
    name = fields.Char('Filename')
    delimeter = fields.Char('Delimeter', default=',',
                            help='Default delimeter is ","')
    info = fields.Text()
    product_categ_id = fields.Many2one(
        comodel_name='product.category')
    stock_route_ids = fields.Many2many(
        comodel_name='stock.location.route'
    )
    processing = fields.Boolean('Processing', default=True)

    _order = 'date DESC'

    @api.multi
    def action_import(self):
        """Load Product data from the CSV file."""
        # Decode the file data
        imports = self.search([
            ('processing', '=', True),
            ('id', '!=', self.id),
        ])
        if imports:
            raise exceptions.Warning(_(
                "Another import %s in progress!"
                "Launch this action later, or delete it." %
                [x.name for x in imports]
            ))
        data = base64.b64decode(self.data)
        file_input = cStringIO.StringIO(data)
        file_input.seek(0)
        reader_info = []
        if self.delimeter:
            delimeter = str(self.delimeter)
        else:
            delimeter = ','
        reader = csv.reader(file_input, delimiter=delimeter,
                            lineterminator='\r\n')
        try:
            reader_info.extend(reader)
        except Exception:
            raise exceptions.Warning(_("Not a valid file!"))
        keys = reader_info[0]
        # check if keys exist
        if not isinstance(keys, list) or ('name' not in keys or
                                          'default_code' not in keys or
                                          'list_price' not in keys):
            raise exceptions.Warning(
                _("Not 'name' or 'default_code' or 'list_price' keys found"))
        del reader_info[0]

        failed = []
        step = 1
        product_tmpl_obj = self.env['product.template']
        product_obj = self.env['product.product']
        start = datetime.now()
        categ_id = self.product_categ_id.id if self.product_categ_id else False
        route_ids = self.stock_route_ids.ids if self.stock_route_ids else False
        for i in range(len(reader_info)):
            field = reader_info[i]
            values = dict(zip(keys, field))
            product_tmpl = product_tmpl_obj.search([
                ('default_code', '=', values['default_code']),
                '|', ('sale_ok', '=', True),
                ('purchase_ok', '=', True)
            ])
            if values['list_price'] and ',' in values['list_price']:
                raise exceptions.ValidationError(
                    'Values must be formatted in English (with . for decimal).'
                    ' Monetary values must be formatted with 2 decimals, '
                    'without "," separator for thousands.')
            val = {
                'type': 'product',
                'purchase_ok': True,
                'sale_ok': True,
                'default_code': values['default_code'],
                'name': values['name'],
                'list_price': values['list_price'].replace(',', ''),
            }
            if route_ids:
                val.update({
                    'route_ids': [(6, 0, route_ids)],
                })
            if categ_id:
                val.update({
                    'categ_id': categ_id,
                })
            if 'weight_net' in values and values['weight_net']:
                val['weight_net'] = values['weight_net'].replace(',', '')
            uom_id = False
            if 'uom' in values and values['uom']:
                # little check for unit possible names
                if values['uom'].lower() in [
                        'pz', 'pce', 'nr', 'n', u'N°', 'n.']:
                    uom_id = self.env.ref('product.product_uom_unit')
                else:
                    uom_id = self.env['product.uom'].search([
                        ('name', '=', values['uom'].lower())
                    ], limit=1)
                if uom_id:
                    val['uom_id'] = val['uom_po_id'] = uom_id.id
                else:
                    # uom not find in database, use default uom
                    val['uom_id'] = val['uom_po_id'] = self.env.ref(
                        'product.product_uom_unit').id

            if product_tmpl:
                if len(product_tmpl.ids) > 1:
                    logging.getLogger(
                        'openerp.addons.product_import').info(
                        'Found duplicated %s product %s code!'
                        % (values['name'], values['default_code']))
                    failed.append('Found duplicated %s product %s code!'
                                  % (values['name'], values['default_code']))
                    continue
                if uom_id and (
                        product_tmpl.uom_id != uom_id or
                        product_tmpl.uom_po_id != uom_id):
                    st_mv_obj = self.env['stock.move']
                    product_lst = product_obj.search([
                        ('product_tmpl_id', '=', product_tmpl.id)])
                    # if no stock move, no problem, else
                    # duplicate product and set not sell/purchasable to old one
                    if st_mv_obj.search([
                            ('product_id', 'in', product_lst.ids)]):
                        new_product = product_tmpl.copy(default={
                            'uom_id': uom_id.id, 'uom_po_id': uom_id.id})
                        product_tmpl.write({
                            'sale_ok': False, 'purchase_ok': False})
                        product_tmpl = new_product
                        logging.getLogger(
                            'openerp.addons.product_import').info(
                            'Duplicated %s product %s code for uom change!'
                            % (values['name'], values['default_code']))
                        failed.append('Duplicated %s product %s code for uom '
                                      'change!' % (values['name'],
                                                   values['default_code']))
                try:
                    product_tmpl.write(val)
                except:
                    logging.getLogger(
                        'openerp.addons.product_import').info(
                        'Updated of %s product %s code failed!'
                        % (values['name'], values['default_code']))
                    failed.append('Updated of %s product %s code failed!'
                                  % (values['name'], values['default_code']))
            else:
                try:
                    product_tmpl_obj.create(val)
                except:
                    logging.getLogger(
                        'openerp.addons.product_import').info(
                        'Creation of %s product %s code failed!'
                        % (values['name'], values['default_code']))
                    failed.append('Creation of %s product %s code failed!'
                                  % (values['name'], values['default_code']))
            if i * 100.0 / len(reader_info) > step:
                logging.getLogger(
                    'openerp.addons.product_import').info(
                    'Execution {0}% '.format(
                        int(i * 100.0 / len(reader_info))))
                step += 1
        logging.getLogger(
            'openerp.addons.product_import').info(failed)
        el = str(datetime.now() - start)
        self.write({
            'info': ('Successfully imported %d records of file %s in %s %s' % (
                len(reader_info), self.name, el[:8],
                ('Records failed: %s' % failed) if failed else '')),
            'name': 'False',
            'processing': False,
        })
