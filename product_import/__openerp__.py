# -*- coding: utf-8 -*-
# (c) 2015 AvanzOSC
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

{
    "name": "Product Import from CSV file",
    "version": "8.0.1.0.0",
    "category": "Generic Modules",
    "license": "AGPL-3",
    "author": "AvanzOSC, Sergio Corato",
    "description": "More usable import with "
                   "bypass of errors with log shown to end user and "
                   "standard selection on category and routes.",
    "contributors": [
    ],
    "website": "http://www.efatto.it",
    "depends": [
        "product",
        "stock",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/product_import.xml",
    ],
    "installable": True,
}
