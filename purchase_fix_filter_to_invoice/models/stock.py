# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp.osv import fields, osv


class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    def _get_to_invoice(self, cr, uid, ids, name, args, context=None):
        res = {}
        for picking in self.browse(cr, uid, ids, context=context):
            res[picking.id] = False
            for move in picking.move_lines:
                if move.purchase_line_id and move.purchase_line_id.order_id.\
                        invoice_method == 'picking':
                    if not move.move_orig_ids:
                        res[picking.id] = True
                elif move.origin_returned_move_id and \
                        move.location_dest_id.usage == 'internal':
                    if not move.move_orig_ids:
                        res[picking.id] = True
        return res

    def _get_picking_to_recompute(self, cr, uid, ids, context=None):
        picking_ids = set()
        for move in self.pool.get('stock.move').browse(cr, uid, ids,
                                                       context=context):
            if move.picking_id:
                if move.purchase_line_id or (
                    move.origin_returned_move_id and
                    move.location_dest_id.usage == 'internal'
                ):
                    picking_ids.add(move.picking_id.id)
        return list(picking_ids)

    _columns = {
        'reception_to_invoice': fields.function(
            _get_to_invoice,
            type='boolean',
            string='Invoiceable on incoming shipment?',
               help='Does the picking contains some moves related to a '
                    'purchase order invoiceable on the receipt?',
               store={
                   'stock.move': (
                       _get_picking_to_recompute,
                       ['purchase_line_id', 'picking_id'], 10),
               }),
    }
