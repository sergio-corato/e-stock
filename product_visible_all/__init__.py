# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from . import models


def pre_init_hook(cr):
    cr.execute("UPDATE product_template set company_id = Null")
