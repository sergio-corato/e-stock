# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api


class StockDdtType(models.Model):
    _inherit = 'stock.ddt.type'

    type_code = fields.Selection(
        related='picking_type_id.code'
    )
    partner_type = fields.Selection(
        selection=[
            ('customer', 'Customer'),
            ('supplier', 'Supplier'),
            ('internal', 'Internal'),
        ], compute='_get_partner_type', store=True)

    @api.one
    @api.depends('picking_type_id')
    def _get_partner_type(self):
        if self.picking_type_id.default_location_dest_id == \
                self.env.ref('stock.stock_location_suppliers') or \
                self.picking_type_id.default_location_src_id == \
                self.env.ref('stock.stock_location_suppliers'):
            self.partner_type = 'supplier'
        elif self.picking_type_id.default_location_dest_id == \
                self.env.ref('stock.stock_location_customers') or \
                self.picking_type_id.default_location_src_id == \
                self.env.ref('stock.stock_location_customers'):
            self.partner_type = 'customer'
        else:
            self.partner_type = 'internal'


class StockPickingPackagePreparation(models.Model):
    _inherit = 'stock.picking.package.preparation'

    partner_type = fields.Selection(related='ddt_type_id.partner_type')
    product_ids = fields.Many2many(
        comodel_name='product.product', relation='rel_sppp_products',
        column1='sppp_id', column2='product_id', string='Products')

    @api.multi
    def create_sppp_line_from_products(self):
        for pack in self:
            products = pack.product_ids
            pack.line_ids.unlink()
            for product in products:
                pack.line_ids.create({
                    'product_id': product.id,
                    'product_uom_qty': product.qty_available,
                    'product_uom': product.uom_id.id,
                    'name': product.name,
                    'package_preparation_id': pack.id,
                    'lot_id': False,
                })
