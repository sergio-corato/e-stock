# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, _, exceptions


class SaleOrder(models.Model):
    _inherit = "sale.order"

    shipping_advise = fields.Selection([
        ('none', 'Choose'),
        ('yes', 'Yes'),
        ('no', 'No')],
        string='Shipping Address is defined?',
        readonly=False,
        required=True,
        default='none',
    )

    @api.model
    def create(self, vals):
        res = super(SaleOrder, self).create(vals)
        if vals.get('shipping_advise', False) == 'none':
            raise exceptions.ValidationError(
                'Please select "yes" or "no" in shipping address definition.')
        return res

    @api.multi
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        for order in self:
            if order.shipping_advise == 'none' or \
                    vals.get('shipping_advise', False) == 'none':
                raise exceptions.ValidationError(
                    'Please select "yes" or "no" in shipping address '
                    'definition.')
        return res
