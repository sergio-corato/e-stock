# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from . import models
import logging
from openerp import SUPERUSER_ID


def post_init_hook(cr, registry):
    sale_order_model = registry['sale.order']
    logging.getLogger('openerp.addons.sale_order_shipping_advise').info(
        'Setting values for shipping advise for existing orders ')
    order_ids = sale_order_model.search(cr, SUPERUSER_ID, [
        ('state', 'not in', ['draft', 'sent'])
    ])
    sale_order_model.write(cr, SUPERUSER_ID, order_ids, {
            'shipping_advise': 'yes',
            })
