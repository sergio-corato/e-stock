# -*- coding: utf-8 -*-
# Copyright (C) 2019 Sergio Corato
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Add shipped visibility purchase order',
    'version': '8.0.1.0.0',
    'category': 'other',
    'author': 'Sergio Corato',
    'description': 'Show shipped in tree and filter of purchase order.',
    'website': 'https://efatto.it',
    'license': 'LGPL-3',
    'depends': [
        'purchase',
        'stock',
    ],
    'data': [
        'views/purchase_view.xml',
    ],
    'installable': True
}
