# -*- coding: utf-8 -*-
##############################################################################
# For copyright and license notices, see __openerp__.py file in root directory
##############################################################################
from openerp import models, fields, api, exceptions, _


class DdTCreateInvoice(models.TransientModel):
    _inherit = "ddt.create.invoice"

    def check_ddt_data(self, ddts):
        res = super(DdTCreateInvoice, self).check_ddt_data(ddts)
        partner_ids = ddts.mapped('partner_id')
        for partner_id in partner_ids:
            ddt_group = ddts.filtered(lambda x: x.partner_id == partner_id)
            if len(set(ddt_group.mapped('partner_shipping_id'))) > 1:
                raise exceptions.ValidationError(
                    _("Selected DDTs of partner %s have different Shipping"
                      " Partners") % partner_id.name)
        return res